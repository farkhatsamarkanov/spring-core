# spring-core

A2 course Spring core task

Instructions to deploy (for Apache Tomcat):

1. Install Apache Tomcat;
2. In the project directory execute following commands: "-mvn compile package"
3. Copy "spring-core-1.0.war" file from /target folder of project directory to webapps folder of Tomcat root directory;
4. Go to  http://localhost:{YourTomcatPortNumber}/spring-core-1.0/ using web browser or Postman for user page
5. The rest of endpoints are implemented as REST API endpoints
6. To get the list of tickets unmarshalled from XML file go to: http://localhost:{YourTomcatPortNumber}/spring-core-1.0/bookedByUser?userId=0
7. To get the PDF file go to: http://localhost:{YourTomcatPortNumber}/spring-core-1.0/bookedByUserPdf?userId=0