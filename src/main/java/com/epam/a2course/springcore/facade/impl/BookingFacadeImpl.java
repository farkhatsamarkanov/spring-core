package com.epam.a2course.springcore.facade.impl;

import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.facade.BookingFacade;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.Ticket.Category;
import com.epam.a2course.springcore.model.User;
import com.epam.a2course.springcore.model.impl.TicketImpl;
import com.epam.a2course.springcore.model.impl.UserImpl;
import com.epam.a2course.springcore.service.EventService;
import com.epam.a2course.springcore.service.TicketService;
import com.epam.a2course.springcore.service.UserService;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Service
public class BookingFacadeImpl implements BookingFacade {
    private static final Logger LOGGER = Logger.getLogger(BookingFacadeImpl.class.getName());

    private final EventService eventService;
    private final TicketService ticketService;
    private final UserService userService;

    public BookingFacadeImpl(EventService eventService, TicketService ticketService,
                             UserService userService) {
        this.eventService = eventService;
        this.ticketService = ticketService;
        this.userService = userService;
    }

    @PostConstruct
    private void preloadTickets() throws Exception {
        FileReader reader = null;
        reader = new FileReader("webapps/spring-core-1.0/WEB-INF/classes/tickets.xml");

        List<TicketImpl> newTickets = (ArrayList<TicketImpl>) Unmarshaller.unmarshal(ArrayList.class, reader);

        for (TicketImpl ticket : newTickets) {
            ticketService.create(ticket);
        }

        User sampleUser = new UserImpl(0L, "Sample name", "sample@mail.com");
        userService.create(sampleUser);
    }

    @Override
    public Event getEventById(Long eventId) {
        LOGGER.info("Trying to fetch event with id: " + eventId);
        return eventService.getById(eventId);
    }

    @Override
    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        LOGGER.info("Trying to fetch events with title: " + title);
        return eventService.getEventsByTitle(title, pageSize, pageNum);
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
        LOGGER.info("Trying to fetch events for day: " + day);
        return eventService.getEventsForDay(day, pageSize, pageNum);
    }

    @Override
    public Event createEvent(Event event) throws AlreadyExistsException {
        LOGGER.info("Trying to create event: " + event);
        return eventService.create(event);
    }

    @Override
    public Event updateEvent(Event event) throws NotFoundException {
        LOGGER.info("Trying to update event: " + event);
        return eventService.update(event);
    }

    @Override
    public boolean deleteEvent(Long eventId) throws NotFoundException {
        LOGGER.info("Trying to delete event with id: " + eventId);
        return eventService.deleteById(eventId);
    }

    @Override
    public User getUserById(Long userId) {
        LOGGER.info("Trying to fetch user with id: " + userId);
        return userService.getById(userId);
    }

    @Override
    public User getUserByEmail(String email) throws NotFoundException {
        LOGGER.info("Trying to fetch user with email: " + email);
        return userService.getByEmail(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        LOGGER.info("Trying to fetch users with name: " + name);
        return userService.getByName(name, pageSize, pageNum);
    }

    @Override
    public User createUser(User user) throws AlreadyExistsException {
        LOGGER.info("Trying to create user: " + user);
        return userService.create(user);
    }

    @Override
    public User updateUser(User user) throws NotFoundException {
        LOGGER.info("Trying to update user: " + user);
        return userService.update(user);
    }

    @Override
    public boolean deleteUser(Long userId) throws NotFoundException {
        LOGGER.info("Trying to delete user with id: " + userId);
        return userService.deleteById(userId);
    }

    @Override
    public Ticket bookTicket(Long userId, Long eventId, int place, Category category) throws AlreadyExistsException {
        Ticket ticket = new TicketImpl();
        ticket.setCategory(category);
        ticket.setUserId(userId);
        ticket.setPlace(place);
        ticket.setEventId(eventId);
        LOGGER.info("Trying to book ticket: " + ticket);
        return ticketService.create(ticket);
    }

    @Override
    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        LOGGER.info("Trying to fetch booked tickets for user: " + user);
        return ticketService.getBookedTickets(user);
    }

    @Override
    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        LOGGER.info("Trying to fetch booked tickets for event: " + event);
        return ticketService.getBookedTickets(event);
    }

    @Override
    public boolean cancelTicket(Long ticketId) throws NotFoundException {
        LOGGER.info("Trying to cancel ticket with id: " + ticketId);
        return ticketService.deleteById(ticketId);
    }
}
