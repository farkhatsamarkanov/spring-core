package com.epam.a2course.springcore.service;

import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;

import java.util.List;

public interface Service <T> {
    T getById(Long id);

    List<T> getAll();

    T create(T entity) throws AlreadyExistsException;

    T update(T entity) throws NotFoundException;

    boolean deleteById(Long id) throws NotFoundException;

}
