package com.epam.a2course.springcore.service.impl;

import com.epam.a2course.springcore.dao.TicketDAO;
import com.epam.a2course.springcore.dao.impl.TicketDAOImpl;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.User;
import com.epam.a2course.springcore.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketDAO ticketDao;

    @Override
    public Ticket getById(Long id) {
        return ticketDao.findById(id);
    }

    @Override
    public List<Ticket> getAll() {
        return ticketDao.findAll();
    }

    @Override
    public Ticket create(Ticket entity) throws AlreadyExistsException {
        return ticketDao.create(entity);
    }

    @Override
    public Ticket update(Ticket entity) throws NotFoundException {
        return ticketDao.update(entity);
    }

    @Override
    public boolean deleteById(Long id) throws NotFoundException {
        return ticketDao.delete(ticketDao.findById(id));
    }

    @Override
    public List<Ticket> getBookedTickets(User user) {
        return ticketDao.findAll().stream()
                .filter(ticket -> ticket.getUserId() == user.getId())
                .collect(Collectors.toList());
    }

    @Override
    public List<Ticket> getBookedTickets(Event event) {
        return ticketDao.findAll().stream()
                .filter(ticket -> ticket.getEventId() == event.getId())
                .collect(Collectors.toList());
    }
}
