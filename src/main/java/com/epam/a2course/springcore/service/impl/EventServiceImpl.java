package com.epam.a2course.springcore.service.impl;

import com.epam.a2course.springcore.dao.EventDAO;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDAO eventDao;

    @Override
    public Event getById(Long id) {
        return eventDao.findById(id);
    }

    @Override
    public List<Event> getAll() {
        return eventDao.findAll();
    }

    @Override
    public Event create(Event entity) throws AlreadyExistsException {
        return eventDao.create(entity);
    }

    @Override
    public Event update(Event entity) throws NotFoundException {
        return eventDao.update(entity);
    }

    @Override
    public boolean deleteById(Long id) throws NotFoundException {
        return eventDao.delete(eventDao.findById(id));
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
        return eventDao.findAll().stream()
                .filter(event -> Objects.equals(event.getDate(), day))
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        return eventDao.findAll().stream()
                .filter(event -> Objects.equals(event.getTitle(), title))
                .collect(Collectors.toList());
    }
}
