package com.epam.a2course.springcore.service;

import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.User;

import java.util.List;

public interface TicketService extends Service<Ticket>{
    List<Ticket> getBookedTickets(User user);

    List<Ticket> getBookedTickets(Event event);
}
