package com.epam.a2course.springcore.service;

import com.epam.a2course.springcore.model.Event;

import java.util.Date;
import java.util.List;

public interface EventService extends Service<Event>{
    List<Event> getEventsForDay(Date day, int pageSize, int pageNum);

    List<Event> getEventsByTitle(String title, int pageSize, int pageNum);
}
