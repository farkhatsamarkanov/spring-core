package com.epam.a2course.springcore.service.impl;

import com.epam.a2course.springcore.dao.UserDAO;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.User;
import com.epam.a2course.springcore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private  UserDAO userDao;

    @Override
    public User getById(Long id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> getAll() {
        return userDao.findAll();
    }

    @Override
    public User create(User entity) throws AlreadyExistsException {
        return userDao.create(entity);
    }

    @Override
    public User update(User entity) throws NotFoundException {
        return userDao.update(entity);
    }

    @Override
    public boolean deleteById(Long id) throws NotFoundException {
        return userDao.delete(userDao.findById(id));
    }

    @Override
    public User getByEmail(String email) throws NotFoundException {
        return userDao.findAll().stream()
                .filter(user -> Objects.equals(user.getEmail(), email))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("User with email " + email + " not found"));
    }

    @Override
    public List<User> getByName(String name, int pageSize, int pageNum) {
        return userDao.findAll().stream()
                .filter(user -> Objects.equals(user.getName(), name))
                .collect(Collectors.toList());
    }
}
