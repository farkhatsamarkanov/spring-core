package com.epam.a2course.springcore.service;

import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.User;

import java.util.List;

public interface UserService extends Service<User>{
    User getByEmail(String email) throws NotFoundException;

    List<User> getByName(String name, int pageSize, int pageNum);
}
