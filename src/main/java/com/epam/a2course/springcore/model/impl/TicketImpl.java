package com.epam.a2course.springcore.model.impl;

import com.epam.a2course.springcore.model.Ticket;

import java.util.Objects;

public class TicketImpl implements Ticket {
    private Long id;
    private long eventId;
    private long userId;
    private Category category;
    private int place;

    public TicketImpl() {
    }

    public TicketImpl(Long id, long eventId, long userId, Category category, int place) {
        this.id = id;
        this.eventId = eventId;
        this.userId = userId;
        this.category = category;
        this.place = place;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getEventId() {
        return eventId;
    }

    @Override
    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Override
    public long getUserId() {
        return userId;
    }

    @Override
    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int getPlace() {
        return place;
    }

    @Override
    public void setPlace(int place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketImpl ticket = (TicketImpl) o;
        return getId() == ticket.getId() && getEventId() == ticket.getEventId() && getUserId() == ticket.getUserId() && getPlace() == ticket.getPlace() && getCategory() == ticket.getCategory();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getEventId(), getUserId(), getCategory(), getPlace());
    }

    @Override
    public String toString() {
        return "TicketImpl{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", userId=" + userId +
                ", category=" + category +
                ", place=" + place +
                '}';
    }
}
