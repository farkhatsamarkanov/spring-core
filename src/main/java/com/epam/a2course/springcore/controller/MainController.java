package com.epam.a2course.springcore.controller;

import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.facade.BookingFacade;
import com.epam.a2course.springcore.facade.impl.BookingFacadeImpl;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.Ticket.Category;
import com.epam.a2course.springcore.model.User;
import com.epam.a2course.springcore.model.impl.EventImpl;
import com.epam.a2course.springcore.model.impl.UserImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class MainController {
    private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());

    @Autowired
    private BookingFacade bookingFacade;

    @RequestMapping("/")
    public ModelAndView root() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping("/event")
    public ResponseEntity<String> getEventById(@RequestParam Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String eventJSON;
        try {
            eventJSON = mapper.writeValueAsString(bookingFacade.getEventById(id));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error getting event with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventJSON, headers, HttpStatus.OK);
    }

    @GetMapping("/eventByTitle")
    public ResponseEntity<String> getEventByTitle(@RequestParam String title) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String eventJSON;
        try {
            eventJSON = mapper.writeValueAsString(bookingFacade.getEventsByTitle(title, 1,1));
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.WARNING, "Error getting event with title: " + title, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventJSON, headers, HttpStatus.OK);
    }

    @GetMapping("/eventByDate")
    public ResponseEntity<String> getEventForDate(@RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date date) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String eventJSON;
        try {
            eventJSON = mapper.writeValueAsString(bookingFacade.getEventsForDay(date, 1,1));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error getting event: ", e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventJSON, headers, HttpStatus.OK);
    }

    @PostMapping("/event")
    public ResponseEntity<String> createEvent(@RequestParam String title,
                                              @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date date) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String eventJSON;
        try {
            Event event = new EventImpl(title, date);
            eventJSON = mapper.writeValueAsString(bookingFacade.createEvent(event));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error creating event with title: " + title, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventJSON, headers, HttpStatus.OK);
    }

    @PutMapping("/event")
    public ResponseEntity<String> updateEvent(@RequestParam Long id, @RequestParam String title,
                                              @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date date) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String eventJSON;
        try {
            Event event = new EventImpl(id, title, date);
            eventJSON = mapper.writeValueAsString(bookingFacade.updateEvent(event));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error updating event with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventJSON, headers, HttpStatus.OK);
    }

    @DeleteMapping("/event")
    public ResponseEntity<String> deleteEvent(@RequestParam Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            bookingFacade.deleteEvent(id);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error deleting event with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Event successfully deleted", headers, HttpStatus.OK);
    }

    @GetMapping("/user")
    public ResponseEntity<String> getUserById(@RequestParam Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String userJSON;
        try {
            userJSON = mapper.writeValueAsString(bookingFacade.getUserById(id));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error getting user with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(userJSON, headers, HttpStatus.OK);
    }

    @GetMapping("/usersByName")
    public ModelAndView getUsersByName(Model model, @RequestParam String name) {
        List<User> usersByName = bookingFacade.getUsersByName(name, 1, 1);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("showUsers");
        model.addAttribute("users", usersByName);
        return modelAndView;
    }

    @GetMapping("/userByEmail")
    public ResponseEntity<String> getUserByEmail(@RequestParam String email) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String userJSON = "";
        try {
             userJSON = mapper.writeValueAsString(bookingFacade.getUserByEmail(email));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error fetching user with email: " + email, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(userJSON, headers, HttpStatus.OK);
    }

    @PostMapping("/user")
    public ModelAndView createUser(RedirectAttributes redirectAttributes, @ModelAttribute("user") UserImpl user) {
        String message = "Success";
        String alert = "alert-success";
        try {
            bookingFacade.createUser(user);
        } catch (AlreadyExistsException e) {
            LOGGER.log(Level.WARNING, "Error creating user: " + user, e);
            message = "Failed";
            alert = "alert-danger";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        redirectAttributes.addFlashAttribute("message", message);
        redirectAttributes.addFlashAttribute("alertClass", alert);
        return modelAndView;
    }

    @PutMapping("/user")
    public ResponseEntity<String> updateUser(@RequestParam Long id, @RequestParam String name, @RequestParam String email) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String userJSON;
        try {
            User user = new UserImpl(id, name, email);
            userJSON = mapper.writeValueAsString(bookingFacade.updateUser(user));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error updating user with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(userJSON, headers, HttpStatus.OK);
    }

    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            bookingFacade.deleteUser(id);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error deleting user with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("User successfully deleted", headers, HttpStatus.OK);
    }

    @PostMapping("/book")
    public ResponseEntity<String> bookTicket(@RequestParam Long userId, @RequestParam Long eventId,
                                             @RequestParam Integer place, @RequestParam("category") Category category) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String ticketJSON;
        Category categoryEnum;
        try {
            ticketJSON = mapper.writeValueAsString(bookingFacade.bookTicket(userId, eventId, place, category));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error booking ticket for user with id: " + userId, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ticketJSON, headers, HttpStatus.OK);
    }

    @GetMapping("/bookedByUser")
    public ResponseEntity<String> getBookedTicketsByUser(@RequestParam Long userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String ticketJSON;
        try {
            User user = bookingFacade.getUserById(userId);
            ticketJSON = mapper.writeValueAsString(bookingFacade.getBookedTickets(user, 1, 1));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error creating JSON for tickets with userId: " + userId, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ticketJSON, headers, HttpStatus.OK);
    }

    @GetMapping("/bookedByUserPdf")
    public ResponseEntity<byte[]> getBookedTicketsByUserPdf(@RequestParam Long userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("ticketsPDF.pdf","ticketsPDF.pdf");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        Document pdfDocument = new Document();
        PdfWriter pdfWriter = null;
        try {
            pdfWriter = PdfWriter.getInstance(pdfDocument, new FileOutputStream("ticketsPDF.pdf"));
            pdfDocument.open();
            User user = bookingFacade.getUserById(userId);
            List<Ticket> tickets = bookingFacade.getBookedTickets(user, 1, 1);
            for (Ticket ticket : tickets) {
                pdfDocument.add(new Paragraph(ticket.toString()));
            }
        } catch (DocumentException | FileNotFoundException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            return new ResponseEntity<>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            pdfDocument.close();
            if (pdfWriter != null) {
                pdfWriter.close();
            }
        }

        byte[] pdfContent;
        try {
            FileInputStream inputStream = new FileInputStream("ticketsPDF.pdf");
            pdfContent = new byte[inputStream.available()];
            inputStream.read(pdfContent);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            return new ResponseEntity<>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(pdfContent, headers, HttpStatus.OK);
    }

    @GetMapping("/bookedByEvent")
    public ResponseEntity<String> getBookedTicketsByEvent(@RequestParam Long eventId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        String ticketsJSON;
        try {
            Event event = bookingFacade.getEventById(eventId);
            ticketsJSON = mapper.writeValueAsString(bookingFacade.getBookedTickets(event, 1, 1));
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.WARNING, "Error creating JSON for tickets with eventId: " + eventId, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ticketsJSON, headers, HttpStatus.OK);
    }

    @DeleteMapping("/ticket")
    public ResponseEntity<String> cancel(@RequestParam Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            bookingFacade.cancelTicket(id);
        } catch (NotFoundException e) {
            LOGGER.log(Level.WARNING, "Error cancelling ticket with id: " + id, e);
            return new ResponseEntity<>(e.getMessage(), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Ticket successfully cancelled", headers, HttpStatus.OK);
    }
}
