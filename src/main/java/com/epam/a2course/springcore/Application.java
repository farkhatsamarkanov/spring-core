/*package com.epam.a2course.springcore;

import com.epam.a2course.springcore.config.ApplicationConfig;
import com.epam.a2course.springcore.controller.MainController;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.facade.BookingFacade;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.User;
import com.epam.a2course.springcore.model.impl.EventImpl;
import com.epam.a2course.springcore.model.impl.UserImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.util.Date;
import java.util.List;

import static com.epam.a2course.springcore.model.Ticket.Category.*;

public class Application {

    public static void main(String[] args) throws AlreadyExistsException {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.scan("com.epam.a2course.springcore");

        BookingFacade bookingFacade = context.getBean(BookingFacade.class);
        MainController mainController = context.getBean(MainController.class);

        ResponseEntity<String> responseEntity = mainController.bookTicket(123L, 1L, 1, "STANDARD");

        System.out.println(responseEntity);
        context.close();
    }
}
*/