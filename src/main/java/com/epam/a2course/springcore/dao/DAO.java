package com.epam.a2course.springcore.dao;

import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;

import java.util.List;

public interface DAO <T> {

    T findById(Long id);

    List<T> findAll();

    T create(T entity) throws AlreadyExistsException;

    T update(T entity) throws NotFoundException;

    boolean delete(T entity) throws NotFoundException;

}
