package com.epam.a2course.springcore.dao;

import com.epam.a2course.springcore.dao.DAO;
import com.epam.a2course.springcore.model.Event;

public interface EventDAO extends DAO<Event> {

}
