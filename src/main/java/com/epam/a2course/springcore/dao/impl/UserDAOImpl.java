package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.UserDAO;
import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAOImpl extends AbstractDAO implements UserDAO {

    @Override
    public User findById(Long id) {
        return inMemoryDB.findUserById(id);
    }

    @Override
    public List<User> findAll() {
        return inMemoryDB.findAllUsers();
    }

    @Override
    public User create(User entity) throws AlreadyExistsException {
        return (User) inMemoryDB.create(entity);
    }

    @Override
    public User update(User entity) throws NotFoundException {
        return (User) inMemoryDB.update(entity);
    }

    @Override
    public boolean delete(User entity) throws NotFoundException {
        return inMemoryDB.delete(entity);
    }
}
