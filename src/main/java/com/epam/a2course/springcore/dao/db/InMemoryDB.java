package com.epam.a2course.springcore.dao.db;

import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import com.epam.a2course.springcore.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class InMemoryDB {

    private static final InMemoryMap dataBase = new InMemoryMap();

    private static final String TICKET_PREFIX = "ticket:";
    private static final String EVENT_PREFIX = "event:";
    private static final String USER_PREFIX = "user:";
    private Long ticketIdCounter = 0L;
    private Long eventIdCounter = 0L;
    private Long userIdCounter = 0L;

    public Ticket findTicketById(Long id) {
        return (Ticket) dataBase.get(id, Ticket.class);
    }

    public User findUserById(Long id) {
        return (User) dataBase.get(id, User.class);
    }

    public Event findEventById(Long id) {
        return (Event) dataBase.get(id, Event.class);
    }

    public List<Ticket> findAllTickets() {
        return dataBase.entrySet().stream()
                .filter(entry -> entry.getKey().contains(TICKET_PREFIX))
                .map(entry -> (Ticket) entry.getValue())
                .collect(Collectors.toList());
    }
    public List<User> findAllUsers() {
        return dataBase.entrySet().stream()
                .filter(entry -> entry.getKey().contains(USER_PREFIX))
                .map(entry -> (User) entry.getValue())
                .collect(Collectors.toList());
    }
    public List<Event> findAllEvents() {
        return dataBase.entrySet().stream()
                .filter(entry -> entry.getKey().contains(EVENT_PREFIX))
                .map(entry -> (Event) entry.getValue())
                .collect(Collectors.toList());
    }

    public Object create(Object entity) throws AlreadyExistsException {
        if (entity instanceof Ticket) {
            if (((Ticket) entity).getId() != null && dataBase.get(((Ticket) entity).getId(), Ticket.class) != null) {
                throw new AlreadyExistsException("Ticket with id " + ((Ticket) entity).getId() + " already exists");
            }
            ((Ticket) entity).setId(generateIdForEntity(entity));
            dataBase.put(entity);
        }
        if (entity instanceof Event) {
            if (((Event) entity).getId() != null && dataBase.get(((Event) entity).getId(), Event.class) != null) {
                throw new AlreadyExistsException("Event with id " + ((Event) entity).getId() + " already exists");
            }
            ((Event) entity).setId(generateIdForEntity(entity));
            dataBase.put(entity);
        }
        if (entity instanceof User) {
            if (((User) entity).getId() != null && dataBase.get(((User) entity).getId(), User.class) != null) {
                throw new AlreadyExistsException("User with id " + ((User) entity).getId() + " already exists");
            }
            ((User) entity).setId(generateIdForEntity(entity));
            dataBase.put(entity);
        }
        return entity;
    }

    public Object update(Object entity) throws NotFoundException {
        if (entity instanceof Ticket) {
            if (dataBase.get(((Ticket) entity).getId(), Ticket.class) == null) {
                throw new NotFoundException("Ticket with id + " + ((Ticket) entity).getId() + " not exists");
            }
            dataBase.put(entity);
        }
        if (entity instanceof Event) {
            if (dataBase.get(((Event) entity).getId(), Event.class) == null) {
                throw new NotFoundException("Event with id + " + ((Event) entity).getId() + " not exists");
            }
            dataBase.put(entity);
        }
        if (entity instanceof User) {
            if (dataBase.get(((User) entity).getId(), User.class) == null) {
                throw new NotFoundException("User with id + " + ((User) entity).getId() + " not exists");
            }
            dataBase.put(entity);
        }
        return entity;
    }

    public boolean delete(Object entity) throws NotFoundException {
        if (entity instanceof Ticket) {
            if (dataBase.get(((Ticket) entity).getId(), Ticket.class) == null) {
                throw new NotFoundException("Ticket with id " + ((Ticket) entity).getId() + " not exists");
            }
            dataBase.remove(((Ticket)entity).getId(), Ticket.class);
        }
        if (entity instanceof Event) {
            if (dataBase.get(((Event) entity).getId(), Event.class) == null) {
                throw new NotFoundException("Event with id " + ((Event) entity).getId() + " not exists");
            }
            dataBase.remove(((Event)entity).getId(), Event.class);
        }
        if (entity instanceof User) {
            if (dataBase.get(((User) entity).getId(), User.class) == null) {
                throw new NotFoundException("User with id " + ((User) entity).getId() + " not exists");
            }
            dataBase.remove(((User)entity).getId(), User.class);
        }
        return true;
    }

    private static String generateId(long id, Class clazz) {
        if (clazz.getName().equals(Ticket.class.getName())) {
            return TICKET_PREFIX + id;
        }
        if (clazz.getName().equals(Event.class.getName())) {
            return EVENT_PREFIX + id;
        }
        if (clazz.getName().equals(User.class.getName())) {
            return USER_PREFIX + id;
        }
        throw new IllegalArgumentException("Incorrect type");
    }

    private static String generateId(Object entity) {
        long id;
        if (entity instanceof Ticket) {
            if (((Ticket) entity).getId() != null) {
                id = ((Ticket) entity).getId();
            } else {
                id = generateIdForEntity(entity);
            }
            return TICKET_PREFIX + id;
        }
        if (entity instanceof Event) {
            if (((Event) entity).getId() != null) {
                id = ((Event) entity).getId();
            } else {
                id = generateIdForEntity(entity);
            }
            return EVENT_PREFIX + id;
        }
        if (entity instanceof User) {
            if (((User) entity).getId() != null) {
                id = ((User) entity).getId();
            } else {
                id = generateIdForEntity(entity);
            }
            return USER_PREFIX + id;
        }
        throw new IllegalArgumentException("Incorrect type");
    }

    private static int generateIdForEntity(Object entity) {
        Collection<?> filteredEntities = dataBase.db.values().stream()
                .filter(e -> e.getClass() == entity.getClass())
                .collect(Collectors.toList());

        return filteredEntities.size();
    }

    static class InMemoryMap {

        private final Map<String, Object> db = new HashMap<>();

        public Object get(Long id, Class clazz) {
            Object entity = db.get(generateId(id, clazz));
            if (clazz.isInstance(entity)) {
                return entity;
            }
            return null;
        }

        public void put(Object entity) {
            db.put(generateId(entity), entity);
        }

        public Set<Map.Entry<String, Object>> entrySet(){
            return db.entrySet();
        }

        public void remove(Long id, Class clazz) {
            db.remove(generateId(id, clazz));
        }
    }
}
