package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.TicketDAO;
import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Ticket;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketDAOImpl extends AbstractDAO implements TicketDAO {

    @Override
    public Ticket findById(Long id) {
        return inMemoryDB.findTicketById(id);
    }

    @Override
    public List<Ticket> findAll() {
        return inMemoryDB.findAllTickets();
    }

    @Override
    public Ticket create(Ticket entity) throws AlreadyExistsException {
        return (Ticket) inMemoryDB.create(entity);
    }

    @Override
    public Ticket update(Ticket entity) throws NotFoundException {
        return (Ticket) inMemoryDB.update(entity);
    }

    @Override
    public boolean delete(Ticket ticket) throws NotFoundException {
        return inMemoryDB.delete(ticket);
    }
}
