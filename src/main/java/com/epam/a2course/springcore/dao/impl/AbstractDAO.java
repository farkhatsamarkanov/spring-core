package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.db.InMemoryDB;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDAO {
    @Autowired
    protected InMemoryDB inMemoryDB;

}
