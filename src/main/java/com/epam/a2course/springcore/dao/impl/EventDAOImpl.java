package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.EventDAO;
import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventDAOImpl extends AbstractDAO implements EventDAO {

    @Override
    public Event findById(Long id) {
        return inMemoryDB.findEventById(id);
    }

    @Override
    public List<Event> findAll() {
        return inMemoryDB.findAllEvents();
    }

    @Override
    public Event create(Event entity) throws AlreadyExistsException {
        return (Event) inMemoryDB.create(entity);
    }

    @Override
    public Event update(Event entity) throws NotFoundException {
        return (Event) inMemoryDB.update(entity);
    }

    @Override
    public boolean delete(Event entity) throws NotFoundException {
        return inMemoryDB.delete(entity);
    }
}
