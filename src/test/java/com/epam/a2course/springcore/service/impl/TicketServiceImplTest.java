package com.epam.a2course.springcore.service.impl;

import com.epam.a2course.springcore.dao.TicketDAO;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import com.epam.a2course.springcore.model.Ticket;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class TicketServiceImplTest {

    @Test
    void testGetById() {
        TicketDAO ticketDao = mock(TicketDAO.class);
        when(ticketDao.findById(anyLong())).thenReturn(mock(Ticket.class));
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        ticketServiceImpl.getById(123L);
        verify(ticketDao).findById(anyLong());
        assertTrue(ticketServiceImpl.getAll().isEmpty());
    }

    @Test
    void testGetAll() {
        TicketDAO ticketDao = mock(TicketDAO.class);
        ArrayList<Ticket> ticketList = new ArrayList<>();
        when(ticketDao.findAll()).thenReturn(ticketList);
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        List<Ticket> actualAll = ticketServiceImpl.getAll();
        assertSame(ticketList, actualAll);
        assertTrue(actualAll.isEmpty());
        verify(ticketDao).findAll();
    }

    @Test
    void testCreate() throws AlreadyExistsException {
        TicketDAO ticketDao = mock(TicketDAO.class);
        when(ticketDao.create(any())).thenReturn(mock(Ticket.class));
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        ticketServiceImpl.create(mock(Ticket.class));
        verify(ticketDao).create(any());
        assertTrue(ticketServiceImpl.getAll().isEmpty());
    }

    @Test
    void testUpdate() throws NotFoundException {
        TicketDAO ticketDao = mock(TicketDAO.class);
        when(ticketDao.update(any())).thenReturn(mock(Ticket.class));
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        ticketServiceImpl.update(mock(Ticket.class));
        verify(ticketDao).update(any());
        assertTrue(ticketServiceImpl.getAll().isEmpty());
    }

    @Test
    void testDeleteById() throws NotFoundException {
        TicketDAO ticketDao = mock(TicketDAO.class);
        when(ticketDao.delete(any())).thenReturn(true);
        when(ticketDao.findById(anyLong())).thenReturn(mock(Ticket.class));
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        assertTrue(ticketServiceImpl.deleteById(123L));
        verify(ticketDao).delete(any());
        verify(ticketDao).findById(anyLong());
        assertTrue(ticketServiceImpl.getAll().isEmpty());
    }

    @Test
    void testGetBookedTickets() {
        TicketDAO ticketDao = mock(TicketDAO.class);
        when(ticketDao.findAll()).thenReturn(new ArrayList<>());
        TicketServiceImpl ticketServiceImpl = new TicketServiceImpl();
        ReflectionTestUtils.setField(ticketServiceImpl, "ticketDao", ticketDao);
        assertTrue(ticketServiceImpl.getBookedTickets(mock(Event.class)).isEmpty());
        verify(ticketDao).findAll();
        assertTrue(ticketServiceImpl.getAll().isEmpty());
    }
}
