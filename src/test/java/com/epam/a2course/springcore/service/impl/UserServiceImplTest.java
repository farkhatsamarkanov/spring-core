package com.epam.a2course.springcore.service.impl;

import com.epam.a2course.springcore.dao.UserDAO;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    @Test
    void testGetById() {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.findById(anyLong())).thenReturn(mock(User.class));
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        userServiceImpl.getById(123L);
        verify(userDao).findById(anyLong());
        assertTrue(userServiceImpl.getAll().isEmpty());
    }

    @Test
    void testGetAll() {
        UserDAO userDao = mock(UserDAO.class);
        ArrayList<User> userList = new ArrayList<>();
        when(userDao.findAll()).thenReturn(userList);
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        List<User> actualAll = userServiceImpl.getAll();
        assertSame(userList, actualAll);
        assertTrue(actualAll.isEmpty());
        verify(userDao).findAll();
    }

    @Test
    void testCreate() throws AlreadyExistsException {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.create(any())).thenReturn(mock(User.class));
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        userServiceImpl.create(mock(User.class));
        verify(userDao).create(any());
        assertTrue(userServiceImpl.getAll().isEmpty());
    }

    @Test
    void testUpdate() throws NotFoundException {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.update(any())).thenReturn(mock(User.class));
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        userServiceImpl.update(mock(User.class));
        verify(userDao).update(any());
        assertTrue(userServiceImpl.getAll().isEmpty());
    }

    @Test
    void testDeleteById() throws NotFoundException {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.delete(any())).thenReturn(true);
        when(userDao.findById(anyLong())).thenReturn(mock(User.class));
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        assertTrue(userServiceImpl.deleteById(123L));
        verify(userDao).delete(any());
        verify(userDao).findById(anyLong());
        assertTrue(userServiceImpl.getAll().isEmpty());
    }

    @Test
    void testGetByEmail() {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.findAll()).thenReturn(new ArrayList<>());
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
        assertThrows(NotFoundException.class, () -> userServiceImpl.getByEmail("jane.doe@example.org"));
        verify(userDao).findAll();
    }

    @Test
    void testGetByName() {
        UserDAO userDao = mock(UserDAO.class);
        when(userDao.findAll()).thenReturn(new ArrayList<>());
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        ReflectionTestUtils.setField(userServiceImpl, "userDao", userDao);
       assertTrue(userServiceImpl.getByName("Name", 3, 10).isEmpty());
        verify(userDao).findAll();
        assertTrue(userServiceImpl.getAll().isEmpty());
    }
}
