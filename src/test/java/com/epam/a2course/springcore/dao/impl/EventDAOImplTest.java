package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Event;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class EventDAOImplTest {

    @Test
    void testFindById() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.findEventById(anyLong())).thenReturn(mock(Event.class));
        EventDAOImpl eventDaoImpl = new EventDAOImpl();
        ReflectionTestUtils.setField(eventDaoImpl, "inMemoryDB", inMemoryDatabase);
        eventDaoImpl.findById(123L);
        verify(inMemoryDatabase).findEventById(anyLong());
        assertTrue(eventDaoImpl.findAll().isEmpty());
    }

    @Test
    void testFindAll() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        ArrayList<Event> eventList = new ArrayList<>();
        when(inMemoryDatabase.findAllEvents()).thenReturn(eventList);
        EventDAOImpl eventDaoImpl = new EventDAOImpl();
        ReflectionTestUtils.setField(eventDaoImpl, "inMemoryDB", inMemoryDatabase);
        List<Event> actualFindAllResult = eventDaoImpl.findAll();
        assertSame(eventList, actualFindAllResult);
        assertTrue(actualFindAllResult.isEmpty());
        verify(inMemoryDatabase).findAllEvents();
    }

    @Test
    void testCreate() throws AlreadyExistsException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.create(any())).thenThrow(
                new AlreadyExistsException("Dummy exception message"));
        EventDAOImpl eventDaoImpl = new EventDAOImpl();
        ReflectionTestUtils.setField(eventDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(AlreadyExistsException.class, () -> eventDaoImpl.create(mock(Event.class)));
        verify(inMemoryDatabase).create(any());
    }

    @Test
    void testUpdate() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.update(any())).thenThrow(
                new NotFoundException("An error occurred"));
        EventDAOImpl eventDaoImpl = new EventDAOImpl();
        ReflectionTestUtils.setField(eventDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(NotFoundException.class, () -> eventDaoImpl.update(mock(Event.class)));
        verify(inMemoryDatabase).update(any());
    }

    @Test
    void testDelete() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.delete(any())).thenReturn(true);
        EventDAOImpl eventDaoImpl = new EventDAOImpl();
        ReflectionTestUtils.setField(eventDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertTrue(eventDaoImpl.delete(mock(Event.class)));
        verify(inMemoryDatabase).delete(any());
        assertTrue(eventDaoImpl.findAll().isEmpty());
    }
}
