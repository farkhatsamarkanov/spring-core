package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.Ticket;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class TicketDAOImplTest {
    @Test
    void testFindById() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.findTicketById(anyLong())).thenReturn(mock(Ticket.class));
        TicketDAOImpl ticketDaoImpl = new TicketDAOImpl();
        ReflectionTestUtils.setField(ticketDaoImpl, "inMemoryDB", inMemoryDatabase);
        ticketDaoImpl.findById(123L);
        verify(inMemoryDatabase).findTicketById(anyLong());
        assertTrue(ticketDaoImpl.findAll().isEmpty());
    }

    @Test
    void testFindAll() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        ArrayList<Ticket> ticketList = new ArrayList<>();
        when(inMemoryDatabase.findAllTickets()).thenReturn(ticketList);
        TicketDAOImpl ticketDaoImpl = new TicketDAOImpl();
        ReflectionTestUtils.setField(ticketDaoImpl, "inMemoryDB", inMemoryDatabase);
        List<Ticket> actualFindAllResult = ticketDaoImpl.findAll();
        assertSame(ticketList, actualFindAllResult);
        assertTrue(actualFindAllResult.isEmpty());
        verify(inMemoryDatabase).findAllTickets();
    }

    @Test
    void testCreate() throws AlreadyExistsException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.create(any())).thenThrow(
                new AlreadyExistsException("Dummy exception message"));
        TicketDAOImpl ticketDaoImpl = new TicketDAOImpl();
        ReflectionTestUtils.setField(ticketDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(AlreadyExistsException.class, () -> ticketDaoImpl.create(mock(Ticket.class)));
        verify(inMemoryDatabase).create(any());
    }

    @Test
    void testUpdate() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.update(any())).thenThrow(
                new NotFoundException("An error occurred"));
        TicketDAOImpl ticketDaoImpl = new TicketDAOImpl();
        ReflectionTestUtils.setField(ticketDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(NotFoundException.class, () -> ticketDaoImpl.update(mock(Ticket.class)));
        verify(inMemoryDatabase).update(any());
    }

    @Test
    void testDelete() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.delete(any())).thenReturn(true);
        TicketDAOImpl ticketDaoImpl = new TicketDAOImpl();
        ReflectionTestUtils.setField(ticketDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertTrue(ticketDaoImpl.delete(mock(Ticket.class)));
        verify(inMemoryDatabase).delete(any());
        assertTrue(ticketDaoImpl.findAll().isEmpty());
    }
}
