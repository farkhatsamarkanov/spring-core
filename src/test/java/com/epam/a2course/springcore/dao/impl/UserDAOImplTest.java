package com.epam.a2course.springcore.dao.impl;

import com.epam.a2course.springcore.dao.db.InMemoryDB;
import com.epam.a2course.springcore.exception.AlreadyExistsException;
import com.epam.a2course.springcore.exception.NotFoundException;
import com.epam.a2course.springcore.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class UserDAOImplTest {

    @Test
    void testFindById() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.findUserById(anyLong())).thenReturn(mock(User.class));
        UserDAOImpl userDaoImpl = new UserDAOImpl();
        ReflectionTestUtils.setField(userDaoImpl, "inMemoryDB", inMemoryDatabase);
        userDaoImpl.findById(123L);
        verify(inMemoryDatabase).findUserById(anyLong());
        assertTrue(userDaoImpl.findAll().isEmpty());
    }

    @Test
    void testFindAll() {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        ArrayList<User> userList = new ArrayList<>();
        when(inMemoryDatabase.findAllUsers()).thenReturn(userList);
        UserDAOImpl userDaoImpl = new UserDAOImpl();
        ReflectionTestUtils.setField(userDaoImpl, "inMemoryDB", inMemoryDatabase);
        List<User> actualFindAllResult = userDaoImpl.findAll();
        assertSame(userList, actualFindAllResult);
        assertTrue(actualFindAllResult.isEmpty());
        verify(inMemoryDatabase).findAllUsers();
    }

    @Test
    void testCreate() throws AlreadyExistsException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.create(any())).thenThrow(
                new AlreadyExistsException("Dummy message"));
        UserDAOImpl userDaoImpl = new UserDAOImpl();
        ReflectionTestUtils.setField(userDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(AlreadyExistsException.class, () -> userDaoImpl.create(mock(User.class)));
        verify(inMemoryDatabase).create(any());
    }

    @Test
    void testUpdate() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.update(any())).thenThrow(
                new NotFoundException("An error occurred"));
        UserDAOImpl userDaoImpl = new UserDAOImpl();
        ReflectionTestUtils.setField(userDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertThrows(NotFoundException.class, () -> userDaoImpl.update(mock(User.class)));
        verify(inMemoryDatabase).update(any());
    }

    @Test
    void testDelete() throws NotFoundException {
        InMemoryDB inMemoryDatabase = mock(InMemoryDB.class);
        when(inMemoryDatabase.delete(any())).thenReturn(true);
        UserDAOImpl userDaoImpl = new UserDAOImpl();
        ReflectionTestUtils.setField(userDaoImpl, "inMemoryDB", inMemoryDatabase);
        assertTrue(userDaoImpl.delete(mock(User.class)));
        verify(inMemoryDatabase).delete(any());
        assertTrue(userDaoImpl.findAll().isEmpty());
    }
}
